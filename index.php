<?php
require("service/chess.php");
require ("service/validator.php");
$table = "";
$balanced_brackets = "";
$balanced_html_text ="";
if (isset($_POST['submit_chess_button'])) {
    $row_number = $_POST['row_number'];
    $column_number = $_POST['column_number'];
    $table_width = $_POST['width_table'];
    $square_width = $_POST['square_width'];
    if($column_number==0)
        $table  = "required column number";
    else if ($row_number ==0)
        $table = "required row number";
    else  $table = drawChess($row_number, $column_number);
}else if(isset($_POST['submit_brackets_button']))
    $balanced_brackets = balancedBrackets($_POST['bracket_string']);
else if (isset($_POST['submit_html_text']))
    $balanced_html_text = balancedHtml($_POST['bracket_string']);




?>
<!DOCTYPE html>
<html>
<head><link rel="stylesheet" href="style/input.css"></head>
<body>
<div>
    <h2>Question number 1 </h2>
    <p>Please add a html string your string that match the questions 1 arguments example: </p>
    <form class="form-inline" action="" method="post">
        <label for="bracket-label">HTML Text</label>
        <input type="text" name="bracket_string"/>
        <input type="submit" name="submit_html_text"/>
        <p><?php echo $balanced_html_text; ?></p>
    </form>
</div>
<div>
    <h2>Question number 2 </h2>
    <p>Please add a string that contain "( [ { } ] )" the following as per question 2 </p>
    <form class="form-inline" action="" method="post">
        <label for="bracket-label">Brackets Text : </label>
        <input type="text" name="bracket_string"/>
        <input type="submit" name="submit_brackets_button"/>
        <p><?php echo $balanced_brackets; ?></p>
    </form>
</div>
<div>
    <h2>Question number 3 : Chess Table Game </h2>
    <p>Please add a row number and column</p>
    <p><i>it will be better to keep the table to be drawn automatically it will automatically fit it is size :)</i></p>
    <p><i>we recommend to keep the table width as it is  but in case your would like to play arround please add your expected table width in the Table width size filed </i></p>
    <form class="form-inline" action="" method="post">
        <label for="row_number">Row number : </label>
        <input type="text" name="row_number"/>
        <label for="row_number">Column number : </label>
        <input type="text" name="column_number"/>
        <input type="submit" name="submit_chess_button"/>
    </form>
    <div><?php echo $table ?></div>

</div>
</body>
</html>





