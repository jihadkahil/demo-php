<?php

/**
 * Name : Draw Chess
 * Role : the role of this function is draw a chess table base on the user input
 * it required :
 * 1.Row Length
 * 2.Column Length
 * optional are:
 * 1.width of the table
 * 2.suare wifth by default is equal to 30
 * @param $row_length
 * @param $column_length
 * @param $width
 * @param $square_width
 */
function drawChess ($row_length,$column_length){
    $table = "<table  cellspacing='0px' cellpadding='0px' border='1px'>";

    for ($row=1; $row <= $row_length; $row++) {
        for($row=1;$row<=$row_length;$row++)
        {


            $table = $table."<tr>";
            for($col=1;$col<=$column_length;$col++)
            {
                $total=$row+$col;
                if($total%2==0)
                {
                    $table=  $table. "<td height=30 width=30 bgcolor=#FFFFFF></td>";
                }
                else
                {
                    $table=  $table. "<td height=30 width=30 bgcolor=#000000></td>";
                }
            }
            $table= $table. "</tr>";
        }
    };



    return $table;

}


?>