<?php

/** Name : Balanced Brackets
 * Role : the role of this function is the read a text that contains ([{}]) in sequence
 * and check that the sequence of open close brackerts are well formatted
 * as well this function will reject all content does not not match the criteria of the question
 * @param $text
 * @return string
 */
function balancedBrackets($text)
{
    //define a regex expression that allow only the following : ( [ { } ] }
    $re = '/^[{\}\(\)\[\]]*$/';
    //define a list of open_brackets
    define("open_brackets", ["(", "[", "{"]);
    //define a list of close bracked
    define("close_brackets", [")", "]", "}"]);
    //define the result as it is ended
    $result = "Balanced";
    //remove all white spaces
    $trim_text = str_replace(' ', '', $text);
    //split text
    $split_text = str_split(trim($trim_text, " "));
    //check if the the text does contain all repected element if it wrong go out else go to the next function
    $reg_checker = preg_match_all($re, $trim_text, $matches, PREG_SET_ORDER, 0);
    if ($reg_checker == 0)
        $result = "Wrong Input";
    else if (sizeof($split_text) % 2 == 0 && $reg_checker == 1) {

        //loop inside the charachers example at 0 you got {
        // in--> open_brackets --> it position is 0
        // in --> close_brackets --> it postion is 0
        // so if the at the end of the word as the same as close_brackets consider it as true and keep looping until you found false
        //if it is not considere it as match
        // not as the every item has to be have an alternative thus you your total number should be even else it does not descrver to be checked
        $size = sizeof($split_text);
        $max_size = $size - 1;
        $loop_to = $size / 2 - 1;
        for ($x = 0; $x <= $loop_to; $x++) {
            $open_brackets_position = array_search($split_text[$x], open_brackets);
            $close_brackets_item = close_brackets[$open_brackets_position];
            $close_brackets_to_check = $split_text[$max_size - $x];
            if (strcmp($close_brackets_item, $close_brackets_to_check) !== 0) {
                $result = "is Not Balanced";
                break;
            }
        }

    } else {
        $result = "is Not Balanced";
    }
    return $result;
}

/**
 * Name : Balanced HTML TAG
 * Role : the ROle of this function is to check wheveter the HTML TAG as set in sequence
 * if the yes return true
 * if nope :
 * check where the item is wrong and the return the TAG needed to be fixed
 * @param $html_string
 * @return mixed|string
 */
function balancedHtml($html_string)
{


    //---find all the tag that an html should be open with <div> and pull it inside the array
    $opening_tag_reg = '/<\w+>/';
    //---find all the tag that an html should be close with example </div> and pull it inside the an array
    $closing_tag_reg = '/(<\/\w+>)/';
    $catched_item = [];
    preg_match_all($opening_tag_reg, $html_string, $opening_tag);
    preg_match_all($closing_tag_reg, $html_string, $closing_tag);

    //the opened tag values after regex
    $opened_tag = $opening_tag[0];

    //the close tags values after regex
    $closed_tag = $closing_tag[0];


    // contcat loops which mean loops the opened tags inside the closed tags
    // for every opened tag check whethere if it exist if it exist inside close tag slice the close tags
    // once is verfied is false which mean does not exist at least the first one then make it false and all to list of catch predefined
    // at the end of there is no catcha that mean every tag has it is alternative "so it true" else "it is false"

    for ($x = 0; $x <= sizeof($opened_tag)-1; $x++) {
        $isVerified = false;
        for($y =0 ; $y <=sizeof($closed_tag)-1;$y++)
        {
            $opened_tag_item = $opened_tag[$x];
            $closed_tag_item = str_replace('/', '', $closed_tag[$y]);
            if(strcmp($opened_tag_item,$closed_tag_item)==0)
            {
                array_splice($closed_tag,$y,1);
                $isVerified = true;
            }

        }
        if(!$isVerified)
        {
            array_push($catched_item,htmlentities($opened_tag[$x]));
        }

    }
    return sizeof($catched_item)>0 ? $catched_item[0] : "true";

}

?>